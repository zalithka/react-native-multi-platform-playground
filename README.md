# React Native Multi-Platform Playground

## Project Disclaimer

this one is big.. **real** big. it's a ridiculous collection of a multitude of different technology stacks that **probably** don't have any right existing in the same repo.

no, seriously.. she big. and this is before installing any custom libraries:

```bash
❯ du -sh node_modules/
1.4G    node_modules/
```

most times, one can get away with installing a CLI tool locally and executing it with `npx`.. however this project is a bit of stickler on this point, and it requires at least `expo-cli` to be installed globally. this is called directly by `nx-react-native-expo`, not through `npx`.

*with all that said*, this "little experiment" should be good fun, and a great learning experience. (: let's begin..

## Nx Workspace Rundown

this workspace contains the following application projects:

* `mobile`, which is a React Native mobile app, generated using `nx-react-native-expo:app`, and enhanced with Expo tooling for easier development.
* `desktop`, which is an Electron app project, generated using `nx-electron:app`, containing only `main` process business logic.
* `desktop-ui`, which is a React Web app, generated using `@nrwl/react:app`, and designated as the UI for the Electron `renderer` process.

>**NOTE**: although the `desktop-ui` application had to be created before it's Electron wrapper application, you do not need to run explicitly it, this is handled by the `nx-electron` plugin.

along with the following library project:

* `main-ui`, generated using `nx-react-native-expo:lib`, will hold custom UI components using React Native Web, which will provide compatibility with both React Native (mobile) and React Web (wrapped in Electron).

>**NOTE**: I will not be creating NPM run scripts to run/build/test these projects, as the "Nx Console" VS Code extension injects clickable links while browsing the `targets` of each project definition in the root `workspace.json` file.

## Immediate Project Plans

the *primary* goal of this workspace is to create a single user interface, which is built and deployed to native applications across all platforms, both mobile and desktop.

to achieve this, the `libs/main-ui` project will become the source of all UI components, comprised of components imported from `react-native-web`, which caters for both React Web (`apps/desktop-ui`) and React Native (`apps/mobile`).

real application content will need to come from different sources, but must be consistent between the desktop and mobile projects..

>..and that's as far as I've got so far. more coming soon. :)
