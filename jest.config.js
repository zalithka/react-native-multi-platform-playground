module.exports = {
  projects: [
    '<rootDir>/libs/main-ui',
    '<rootDir>/apps/mobile',
    '<rootDir>/apps/desktop-ui',
    '<rootDir>/apps/desktop',
  ],
};
